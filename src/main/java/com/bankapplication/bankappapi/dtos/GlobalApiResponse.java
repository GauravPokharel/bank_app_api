package com.bankapplication.bankappapi.dtos;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GlobalApiResponse {
    private boolean status;

    private String message;

    private Object data;
}
