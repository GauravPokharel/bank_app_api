package com.bankapplication.bankappapi.dtos;

import com.bankapplication.bankappapi.entities.Customer;
import com.bankapplication.bankappapi.enums.AccountType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountDto {
    private int id;

    private String accountNumber;

    private LocalDate openDate;

    private LocalDate closeDate;

    private double balance;

    private AccountType accountType;

    private Customer customer;
}
