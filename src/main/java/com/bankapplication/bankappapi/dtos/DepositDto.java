package com.bankapplication.bankappapi.dtos;

import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DepositDto {
    @NotBlank(message = "Account number can't be blank")
    private String accountNumber;

    @Min(value = 2, message = "Transaction of less than Rs.10 can not be performed.")
    @Positive(message = "Invalid amount")
    private double amount;
}
