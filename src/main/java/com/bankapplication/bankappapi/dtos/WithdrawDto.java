package com.bankapplication.bankappapi.dtos;

import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WithdrawDto {
    @NotBlank(message = "Account number can't be blank")
    private String accountNumber;

    @Min(value = 10, message = "Transaction of less than Rs.10 can not be performed.")
    @Positive(message = "Invalid amount")
    private double amount;
}
