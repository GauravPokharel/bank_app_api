package com.bankapplication.bankappapi.dtos.master;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Getter
@Setter
public class MasterIdTypeDto {
    private int id;
    @NotNull
    @Column(unique = true)
    private String name;
    
    @NotBlank(message = "Code of identity type can't be empty")
    @NotNull
    @Size(min = 2, max = 5, message = "Size of code should be between 2 and 5")
    @Positive(message = "Code should be greater than 10")
    @Column(unique = true)
    private String code;
}
