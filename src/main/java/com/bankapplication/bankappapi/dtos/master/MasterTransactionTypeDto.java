package com.bankapplication.bankappapi.dtos.master;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class MasterTransactionTypeDto {
    private int id;
    @Column(name = "name")
    @NotBlank(message = "Name of transaction type can't be empty")

    private String name;

    @Column(name = "code", unique = true)
    @NotBlank(message = "Code of transaction type can't be empty")
    private String code;
}
