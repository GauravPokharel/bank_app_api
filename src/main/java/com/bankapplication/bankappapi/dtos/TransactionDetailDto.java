package com.bankapplication.bankappapi.dtos;

import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDetailDto {
    private int id;

    private String transactionType;

    @NotBlank(message = "Account number can't be blank")
    private String accountNumber;

    @Min(value = 10, message = "Transaction of less than Rs.10 can not be performed.")
    @Positive(message = "Invalid amount")
    private double amount;

    private LocalDate dateOfTransaction;

    private String toAccountNumber;

    @Size(min = 10, max = 10, message = "Mobile number should of 10 digits")
    @Positive(message = "Invalid mobile number")
    private String mobileNumber;
}
