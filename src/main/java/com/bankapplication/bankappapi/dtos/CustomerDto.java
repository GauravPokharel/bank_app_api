package com.bankapplication.bankappapi.dtos;

import com.bankapplication.bankappapi.enums.AccountType;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDto {
    private int id;

    @NotBlank(message = "Name of customer can't be empty")
    private String fullName;

    @Size(min = 10, max = 10, message = "Mobile number should of 10 digits")
    @Positive(message = "Invalid mobile number")
    private String mobileNumber;

    @Email(message = "Input should be in email format")
    @NotBlank(message = "Email address of customer can't be empty")
    private String emailAddress;

    @NotBlank(message = "Address of customer type can't be empty")
    @Size(min = 3, max = 50, message = "Size must be between 3-50")
    private String address;

    private String googlePlusCode;

    private boolean isPremium;

    @Past
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "Date of birth field is mandatory")
    private LocalDate dateOfBirth;

    @NotNull(message = "masterIdTypeCode cannot ve empty")
    private String masterIdTypeCode;

    @NotNull(message = "Identity code can't empty")
    private String idValue;

    private AccountType accountType;
    
    private String parentName;
}
