package com.bankapplication.bankappapi.controllers.master;

import com.bankapplication.bankappapi.abstracts.BaseClass;
import com.bankapplication.bankappapi.dtos.GlobalApiResponse;
import com.bankapplication.bankappapi.entities.master.MasterTransactionType;
import com.bankapplication.bankappapi.servicies.master.MasterTransactionTypeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("mastertransactiontype")
public class MasterTransactionTypeController extends BaseClass {
    private final MasterTransactionTypeService masterTransactionTypeService;

    public MasterTransactionTypeController(MasterTransactionTypeService masterTransactionTypeService) {
        this.masterTransactionTypeService = masterTransactionTypeService;
    }

    @GetMapping
    public ResponseEntity<GlobalApiResponse> findAll() {
        return new ResponseEntity<>(successResponse("Data fetched successfully.", masterTransactionTypeService.getAllMasterTransactionType()), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<GlobalApiResponse> create(@Valid @RequestBody MasterTransactionType masterTransactionType, BindingResult bindingResult) throws BindException {
        if (!bindingResult.hasErrors()) {
            masterTransactionTypeService.saveMasterTransactionType(masterTransactionType);
            return new ResponseEntity<>(successResponse("Master transaction type created.", masterTransactionType), HttpStatus.OK);
        } else {
            throw new BindException(bindingResult);
        }
    }

    @PutMapping
    public ResponseEntity<GlobalApiResponse> update(@Valid @RequestBody MasterTransactionType masterTransactionType, BindingResult bindingResult) throws BindException {
        if (!bindingResult.hasErrors()) {
            if (masterTransactionType.getId() == 0)
                return new ResponseEntity<>(errorResponse("Master transaction type doesn't contain id.", masterTransactionType), HttpStatus.BAD_REQUEST);
            if (!masterTransactionTypeService.checkMasterTransactionTypeById(masterTransactionType.getId()))
                return new ResponseEntity<>(errorResponse("There is not such master transaction type (Invalid Id).", masterTransactionType), HttpStatus.BAD_REQUEST);
            MasterTransactionType masterTransactionTypeData = masterTransactionTypeService.updateMasterTransactionType(masterTransactionType);
            return new ResponseEntity<>(successResponse("Master transaction type updated (Code will not be updated).", masterTransactionTypeData), HttpStatus.OK);
        } else {
            throw new BindException(bindingResult);
        }
    }

    @GetMapping("/getById/{id}")
    public ResponseEntity<GlobalApiResponse> getById(@PathVariable("id") Integer id) {
        if (!masterTransactionTypeService.checkMasterTransactionTypeById(id))
            return new ResponseEntity<>(errorResponse("There is not such master transaction type (Invalid Id).", id), HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(successResponse("Master transaction type fetched successfully.", masterTransactionTypeService.getMasterTransactionTypeById(id)), HttpStatus.OK);
    }
}
