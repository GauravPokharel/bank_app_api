package com.bankapplication.bankappapi.controllers.master;

import com.bankapplication.bankappapi.abstracts.BaseClass;
import com.bankapplication.bankappapi.dtos.GlobalApiResponse;
import com.bankapplication.bankappapi.entities.master.MasterIdType;
import com.bankapplication.bankappapi.servicies.master.MasterIdTypeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("masteridtype")
public class MasterIdTypeController extends BaseClass {
    private final MasterIdTypeService masterIdTypeService;

    MasterIdTypeController(MasterIdTypeService masterIdTypeService) {
        this.masterIdTypeService = masterIdTypeService;
    }

    @GetMapping
    public ResponseEntity<GlobalApiResponse> findAll() {
        return new ResponseEntity<>(successResponse("Data fetched successfully", masterIdTypeService.getAllMasterIdType()), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<GlobalApiResponse> create(@Valid @RequestBody MasterIdType masterIdType, BindingResult bindingResult) throws BindException {
        if (!bindingResult.hasErrors()) {
            masterIdTypeService.saveMasterIdType(masterIdType);
            return new ResponseEntity<>(successResponse("Data saved successfully", masterIdType
            ), HttpStatus.OK);
        } else {
            throw new BindException(bindingResult);
        }
    }

    @PutMapping
    public ResponseEntity<GlobalApiResponse> update(@Valid @RequestBody MasterIdType masterIdType, BindingResult bindingResult) throws BindException {
        if (!bindingResult.hasErrors()) {
            if (!masterIdTypeService.checkId(masterIdType.getId())) {
                return new ResponseEntity<>(errorResponse("Id doesn't matched", masterIdType
                ), HttpStatus.BAD_REQUEST);
            }
            MasterIdType masterIdType1 = masterIdTypeService.updateMasterIdType(masterIdType);

            return new ResponseEntity<>(successResponse("Data updated successfully (Note:code cannot be changed)", masterIdType1
            ), HttpStatus.OK);
        } else {
            throw new BindException(bindingResult);
        }
    }

    @GetMapping("/getById/{id}")
    public ResponseEntity<GlobalApiResponse> getById(@PathVariable(value = "id") int id) {
        if (!masterIdTypeService.checkId(id)) {
            return new ResponseEntity<>(errorResponse("Master Id type with such id doesn't found", id), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(successResponse("Data fetched successfully", masterIdTypeService.getMasterIdTypeById(id)), HttpStatus.OK);
    }
}
