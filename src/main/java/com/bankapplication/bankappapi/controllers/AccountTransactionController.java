package com.bankapplication.bankappapi.controllers;

import com.bankapplication.bankappapi.abstracts.BaseClass;
import com.bankapplication.bankappapi.dtos.*;
import com.bankapplication.bankappapi.servicies.AccountService;
import com.bankapplication.bankappapi.servicies.AccountTransactionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("accountTransaction")
public class AccountTransactionController extends BaseClass {
    private final AccountTransactionService accountTransactionService;
    private final AccountService accountService;

    public AccountTransactionController(AccountTransactionService accountTransactionService, AccountService accountService) {
        this.accountTransactionService = accountTransactionService;
        this.accountService = accountService;
    }

    @GetMapping()
    public ResponseEntity<GlobalApiResponse> getAll() {
        return new ResponseEntity<>(successResponse("Data fetch successfully.", accountTransactionService.getAllAccountTransaction()), HttpStatus.OK);
    }

    @GetMapping("getAccountTransactionDetails/{id}")
    public ResponseEntity<GlobalApiResponse> getDetails(@PathVariable("id") Integer id) {
        TransactionDetailDto transactionDetailDto = accountTransactionService.getAccountTransactionById(id);
        if (transactionDetailDto == null)
            return new ResponseEntity<>(errorResponse("Account transaction with such id doesn't exists.", id), HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(successResponse("Account transaction fetched successfully.", transactionDetailDto), HttpStatus.OK);
    }

    @PostMapping("/deposit")
    public ResponseEntity<GlobalApiResponse> performDeposit(@Valid @RequestBody DepositDto depositDto, BindingResult bindingResult) throws BindException {
        if (!bindingResult.hasErrors()) {
            if (!accountService.isAccountNumberValid(depositDto.getAccountNumber()))
                return new ResponseEntity<>(errorResponse("Invalid account number.", depositDto), HttpStatus.BAD_REQUEST);
            accountTransactionService.depositTransaction(depositDto);
            return new ResponseEntity<>(successResponse("Deposit transaction completed successfully.", depositDto), HttpStatus.OK);
        } else {
            throw new BindException(bindingResult);
        }
    }

    @PostMapping("/withdraw")
    public ResponseEntity<GlobalApiResponse> performWithdraw(@Valid @RequestBody WithdrawDto withdrawDto, BindingResult bindingResult) throws BindException {
        if (!bindingResult.hasErrors()) {
            if (!accountService.isAccountNumberValid(withdrawDto.getAccountNumber()))
                return new ResponseEntity<>(errorResponse("Invalid account number.", withdrawDto), HttpStatus.BAD_REQUEST);
            if (accountTransactionService.withdrawTransaction(withdrawDto))
                return new ResponseEntity<>(successResponse("Withdraw transaction completed successfully.", withdrawDto), HttpStatus.OK);
            return new ResponseEntity<>(errorResponse("Account doesn't have sufficient balance to perform withdraw transaction.", withdrawDto), HttpStatus.BAD_REQUEST);
        } else {
            throw new BindException(bindingResult);
        }
    }

    @PostMapping("/topUp")
    public ResponseEntity<GlobalApiResponse> performTopUp(@Valid @RequestBody TopUpDto topUpDto, BindingResult bindingResult) throws BindException {
        if (!bindingResult.hasErrors()) {
            if (!accountService.isAccountNumberValid(topUpDto.getAccountNumber()))
                return new ResponseEntity<>(errorResponse("Invalid account number.", topUpDto), HttpStatus.BAD_REQUEST);
            if (accountTransactionService.topUpTransaction(topUpDto))
                return new ResponseEntity<>(successResponse("TopUp transaction completed successfully.", topUpDto), HttpStatus.OK);
            return new ResponseEntity<>(errorResponse("Account doesn't have sufficient balance to perform TopUp transaction.", topUpDto), HttpStatus.BAD_REQUEST);
        } else {
            throw new BindException(bindingResult);
        }
    }

    @PostMapping("/ibft")
    public ResponseEntity<GlobalApiResponse> performIBFT(@Valid @RequestBody IBFTDto ibftDto, BindingResult bindingResult) throws BindException {
        if (!bindingResult.hasErrors()) {

            if (!(accountService.isAccountNumberValid(ibftDto.getFromAccountNumber()) && accountService.isAccountNumberValid(ibftDto.getToAccountNumber())))
                return new ResponseEntity<>(errorResponse("Invalid account number.", ibftDto), HttpStatus.BAD_REQUEST);
            if (accountTransactionService.ibftTransaction(ibftDto))
                return new ResponseEntity<>(successResponse("IBFT transaction completed successfully.", ibftDto), HttpStatus.OK);
            return new ResponseEntity<>(errorResponse("Account doesn't have sufficient balance to perform IBFT transaction.", ibftDto), HttpStatus.BAD_REQUEST);
        } else {
            throw new BindException(bindingResult);
        }
    }

}
