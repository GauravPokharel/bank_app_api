package com.bankapplication.bankappapi.controllers;

import com.bankapplication.bankappapi.abstracts.BaseClass;
import com.bankapplication.bankappapi.dtos.CustomerDto;
import com.bankapplication.bankappapi.dtos.GlobalApiResponse;
import com.bankapplication.bankappapi.entities.Customer;
import com.bankapplication.bankappapi.servicies.CustomerService;
import com.bankapplication.bankappapi.validators.CustomerValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("customer")
public class CustomerController extends BaseClass {
    private final CustomerService customerService;
    private final CustomerValidator customerValidator;

    CustomerController(CustomerService customerService, CustomerValidator customerValidator) {
        this.customerService = customerService;
        this.customerValidator = customerValidator;
    }

    @GetMapping
    public ResponseEntity<GlobalApiResponse> customerIndex() {
        return new ResponseEntity<>(successResponse("Data fetched Successfully", customerService.getAllCustomer()), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<GlobalApiResponse> create(@Valid @RequestBody CustomerDto customerDto, BindingResult bindingResult) throws BindException {
        if (!bindingResult.hasErrors()) {
            if (customerValidator.isValid(customerDto)) {
                if (customerService.isMinor(customerDto.getDateOfBirth()) && !customerService.isParentSet(customerDto)) {
                    return new ResponseEntity<>(errorResponse("It is minor account so please send Parents info too..", customerDto
                    ), HttpStatus.BAD_REQUEST);
                }
                if (customerService.isMinor(customerDto.getDateOfBirth()) && !customerValidator.isParentValid(customerDto.getParentName())) {
                    return new ResponseEntity<>(errorResponse("Please provide valid parents name ", customerDto
                    ), HttpStatus.BAD_REQUEST);
                }
                customerService.saveCustomer(customerDto);
                return new ResponseEntity<>(successResponse("Data saved successfully", customerDto
                ), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(errorResponse(customerValidator.getInvalidMessage(customerDto), customerDto), HttpStatus.BAD_REQUEST);
            }
        } else {
            throw new BindException(bindingResult);
        }
    }

    @PutMapping
    public ResponseEntity<GlobalApiResponse> update(@Valid @RequestBody CustomerDto customerDto) {
        if (!customerService.checkId(customerDto.getId())) {
            return new ResponseEntity<>(errorResponse("Id doesn't matched", customerDto
            ), HttpStatus.BAD_REQUEST);
        }
        if (customerValidator.isUpdateValid(customerDto)) {
            if (customerService.isMinor(customerDto.getDateOfBirth()) && !customerService.isParentSet(customerDto)) {
                return new ResponseEntity<>(errorResponse("It is minor account so please send Parents info too..", customerDto
                ), HttpStatus.BAD_REQUEST);
            }
            if (customerService.isMinor(customerDto.getDateOfBirth()) && !customerValidator.isParentValid(customerDto.getParentName())) {
                return new ResponseEntity<>(errorResponse("Please provide valid parents name ", customerDto
                ), HttpStatus.BAD_REQUEST);
            }
            Customer customer = customerService.updateCustomer(customerDto);
            return new ResponseEntity<>(successResponse("Data updated successfully", customer
            ), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(errorResponse(customerValidator.getUpdateInvalidMessage(customerDto), customerDto), HttpStatus.BAD_REQUEST);
        }
    }
}
