package com.bankapplication.bankappapi.controllers;

import com.bankapplication.bankappapi.abstracts.BaseClass;
import com.bankapplication.bankappapi.dtos.GlobalApiResponse;
import com.bankapplication.bankappapi.entities.Account;
import com.bankapplication.bankappapi.servicies.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// Controller + ResponseBody
@RestController
@RequestMapping("account")
public class AccountController extends BaseClass {
    private final AccountService accountService;

    AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping
    public ResponseEntity<GlobalApiResponse> findAll() {
        return new ResponseEntity<>(successResponse("Data fetched successfully.", accountService.getAllAccount()), HttpStatus.OK);
    }

    @GetMapping("/accountDetails/{id}")
    public ResponseEntity<GlobalApiResponse> getById(@PathVariable("id") Integer id) {
        Account account = accountService.getAccountById(id);
        if (account == null)
            return new ResponseEntity<>(errorResponse("Account with such id is not available.", id), HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(successResponse("Data fetched successfully.", account), HttpStatus.OK);
    }
}
