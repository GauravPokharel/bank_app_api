package com.bankapplication.bankappapi.abstracts;

import com.bankapplication.bankappapi.dtos.GlobalApiResponse;

public class BaseClass {
    protected GlobalApiResponse successResponse(String message, Object object) {
        GlobalApiResponse globalApiResponse = new GlobalApiResponse();
        globalApiResponse.setStatus(true);
        globalApiResponse.setMessage(message);
        globalApiResponse.setData(object);
        return globalApiResponse;
    }

    protected GlobalApiResponse errorResponse(String message, Object object) {
        return GlobalApiResponse.builder()
                .status(false)
                .message(message)
                .data(object)
                .build();
    }
}
