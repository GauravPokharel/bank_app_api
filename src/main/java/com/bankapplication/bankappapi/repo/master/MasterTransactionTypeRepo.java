package com.bankapplication.bankappapi.repo.master;

import com.bankapplication.bankappapi.entities.master.MasterTransactionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterTransactionTypeRepo extends JpaRepository<MasterTransactionType, Integer> {
}
