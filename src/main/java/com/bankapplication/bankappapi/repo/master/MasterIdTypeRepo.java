package com.bankapplication.bankappapi.repo.master;

import com.bankapplication.bankappapi.entities.master.MasterIdType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterIdTypeRepo extends JpaRepository<MasterIdType, Integer> {
}
