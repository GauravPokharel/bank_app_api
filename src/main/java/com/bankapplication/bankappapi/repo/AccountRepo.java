package com.bankapplication.bankappapi.repo;

import com.bankapplication.bankappapi.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepo extends JpaRepository<Account, Integer> {
   /* @Override
    @Query(value = "select * from tbl_account where id=20", nativeQuery = true)
    List<Account> findAll();*/

    Optional<Account> findAccountByAccountNumber(String accountNumber);
    Optional<Account> findAccountById(int id);
}
