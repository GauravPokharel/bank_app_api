package com.bankapplication.bankappapi.repo;

import com.bankapplication.bankappapi.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Integer> {
    Optional<Customer> findCustomerByMobileNumber(String number);

    Optional<Customer> findCustomerByMobileNumberAndIdNot(String number, Integer id);

    List<Customer> findCustomerByEmailAddress(String emailAddress);

    Optional<Customer> findCustomerByEmailAddressAndIdNot(String emailAddress, Integer id);

    Optional<Customer> findCustomerByIdValue(String idValue);

    Optional<Customer> findCustomerById(int id);
}
