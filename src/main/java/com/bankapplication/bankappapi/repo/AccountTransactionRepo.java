package com.bankapplication.bankappapi.repo;

import com.bankapplication.bankappapi.entities.AccountTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountTransactionRepo extends JpaRepository<AccountTransaction, Integer> {
    Optional<AccountTransaction> findAccountTransactionById(int id);
}
