package com.bankapplication.bankappapi.validators;

import com.bankapplication.bankappapi.dtos.CustomerDto;
import com.bankapplication.bankappapi.servicies.CustomerService;
import org.springframework.stereotype.Component;

@Component
public class CustomerValidator {
    private final CustomerService customerService;

    CustomerValidator(CustomerService customerService) {
        this.customerService = customerService;
    }

    public boolean mobileNumberValidate(CustomerDto customerDto) {
        /*List<Customer> customerList = customerService.getAllCustomer();
        Optional<Customer> optional = customerList.stream().filter(i -> i.getMobileNumber().equals(customerDto.getMobileNumber())).findFirst();
        if (optional.isPresent()) {
            return false;
        }
        return true;*/
        return !customerService.checkCustomerByMobileNumber(customerDto.getMobileNumber(), null);
    }

    public boolean mobileNumberUpdateValidate(CustomerDto customerDto) {
        return !customerService.checkCustomerByMobileNumber(customerDto.getMobileNumber(), customerDto.getId());
    }

    public boolean emailValidate(CustomerDto customerDto) {

        return !customerService.checkCustomerByEmailAddress(customerDto.getEmailAddress(), null);
    }

    public boolean emailUpdateValidate(CustomerDto customerDto) {
        return !customerService.checkCustomerByEmailAddress(customerDto.getEmailAddress(), customerDto.getId());
    }

    public boolean identityNumberValidate(CustomerDto customerDto) {

        return !customerService.checkCustomerByIdentityValue(customerDto.getIdValue());
    }

    public boolean isUpdateValid(CustomerDto customerDto) {
        return mobileNumberUpdateValidate(customerDto) && emailUpdateValidate(customerDto);
    }

    public boolean isValid(CustomerDto customerDto) {
        return mobileNumberValidate(customerDto) && emailValidate(customerDto) && identityNumberValidate(customerDto);
    }

    public String getInvalidMessage(CustomerDto customerDto) {
        String message = "";
        if (!mobileNumberValidate(customerDto)) {
            message = message + "Mobile Number already used. ";
        }
        if (!emailValidate(customerDto)) {
            message = message + "Email Address already used. ";
        }
        if (!identityNumberValidate(customerDto)) {
            message = message + "Identity value already used. ";
        }
        return message;
    }

    public String getUpdateInvalidMessage(CustomerDto customerDto) {
        String message = "";
        if (!mobileNumberUpdateValidate(customerDto)) {
            message = message + "Mobile Number already used. ";
        }
        if (!emailUpdateValidate(customerDto)) {
            message = message + "Email Address already used. ";
        }
        return message;
    }


    public boolean isParentValid(String s) {
        int l = 0; // counter for number of letters
        int sp = 0; // counter for number of spaces
        if (s == null) // checks if the String is null
        {
            return false;
        }
        int len = s.length();
        for (int i = 0; i < len; i++) {
            if ((Character.isLetter(s.charAt(i)) == true)) {
                l++;
            }
            if (s.charAt(i) == ' ') {
                sp++;
            }
        }
        return !(sp == 0 || l == 0); // even if one of them is zero then returns false

    }
}
