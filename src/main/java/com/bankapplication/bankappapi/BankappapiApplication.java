package com.bankapplication.bankappapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankappapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankappapiApplication.class, args);
    }

}
