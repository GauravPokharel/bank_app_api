package com.bankapplication.bankappapi.servicies;

import com.bankapplication.bankappapi.dtos.CustomerDto;
import com.bankapplication.bankappapi.entities.Customer;

import java.time.LocalDate;
import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomer();

    Customer saveCustomer(CustomerDto customerDto);

    Customer updateCustomer(CustomerDto customerDto);

    Customer getCustomerById(int id);

    boolean checkId(int id);

    boolean checkCustomerByMobileNumber(String number, Integer id);

    boolean checkCustomerByEmailAddress(String emailAddress,Integer id);

    boolean checkCustomerByIdentityValue(String idValue);

    int calculateAge(LocalDate birthDate);

    boolean isParentSet(CustomerDto customerDto);

    boolean isMinor(LocalDate birthDate);

    List<Customer> searchCustomerByName(String searchValue);
}
