package com.bankapplication.bankappapi.servicies;

import com.bankapplication.bankappapi.entities.Account;
import com.bankapplication.bankappapi.enums.AccountType;

import java.util.List;

public interface AccountService {
    void saveAccount(Account account);

    List<Account> getAllAccount();

    Account getAccountById(int id);

    Account createAccount(AccountType accountType);

    Account getAccountByAccountNumber(String accountNumber);

    boolean isAccountNumberValid(String accountNumber);
}
