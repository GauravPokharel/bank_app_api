package com.bankapplication.bankappapi.servicies.master;

import com.bankapplication.bankappapi.entities.master.MasterIdType;

import java.util.List;


public interface MasterIdTypeService {
    List<MasterIdType> getAllMasterIdType();

    void saveMasterIdType(MasterIdType masterIdType);

    MasterIdType getMasterIdTypeById(int id);

    boolean checkId(int id);

    MasterIdType updateMasterIdType(MasterIdType masterIdType);

    MasterIdType getMasterIdTypeByCode(String code);

}
