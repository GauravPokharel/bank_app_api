package com.bankapplication.bankappapi.servicies.master;

import com.bankapplication.bankappapi.entities.master.MasterTransactionType;

import java.util.List;

public interface MasterTransactionTypeService {
    List<MasterTransactionType> getAllMasterTransactionType();

    MasterTransactionType getMasterTransactionTypeById(int id);

    Boolean checkMasterTransactionTypeById(int id);

    void saveMasterTransactionType(MasterTransactionType masterTransactionType);

    MasterTransactionType updateMasterTransactionType(MasterTransactionType masterTransactionType);

    MasterTransactionType getMasterTransactionTypeByCode(String code);
}
