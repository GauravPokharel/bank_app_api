package com.bankapplication.bankappapi.servicies.master.impl;

import com.bankapplication.bankappapi.entities.master.MasterIdType;
import com.bankapplication.bankappapi.repo.master.MasterIdTypeRepo;
import com.bankapplication.bankappapi.servicies.master.MasterIdTypeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MasterIdTypeServiceImpl implements MasterIdTypeService {
    private final MasterIdTypeRepo masterIdTypeRepo;

    MasterIdTypeServiceImpl(MasterIdTypeRepo masterIdTypeRepo) {
        this.masterIdTypeRepo = masterIdTypeRepo;
    }

    @Override
    public List<MasterIdType> getAllMasterIdType() {
        return masterIdTypeRepo.findAll();
    }

    @Override
    public void saveMasterIdType(MasterIdType masterIdType) {
        masterIdTypeRepo.save(masterIdType);

    }

    @Override
    public MasterIdType getMasterIdTypeById(int id) {
        MasterIdType masterIdType = masterIdTypeRepo.getById(id);
        return masterIdType;
    }

    @Override
    public boolean checkId(int id) {
        List<MasterIdType> masterIdTypeList = getAllMasterIdType();
        Optional<MasterIdType> optional = masterIdTypeList.stream().filter(i -> i.getId() == id).findAny();
        if (optional.isPresent()) {
            return true;
        }
        return false;
    }

    @Override
    public MasterIdType updateMasterIdType(MasterIdType masterIdType) {
        int id = masterIdType.getId();
        MasterIdType masterIdType1 = getMasterIdTypeById(id);
        MasterIdType masterIdType2 = new MasterIdType();
        masterIdType2.setId(id);
        masterIdType2.setCode(masterIdType1.getCode());
        masterIdType2.setName(masterIdType.getName());
        return masterIdTypeRepo.save(masterIdType2);
       /* if(masterIdType.getName().toUpperCase()=="DRIVING LICENSE"){
            masterIdType.setCode("101");
        }
        else if(masterIdType.getName().toUpperCase()=="CITIZENSHIP"){
            masterIdType.setCode("102");
        }
        else if(masterIdType.getName().toUpperCase()=="PAN")
        {
            masterIdType.setCode("103");
        }*/

    }

    @Override
    public MasterIdType getMasterIdTypeByCode(String code) {
        List<MasterIdType> masterIdTypeList= getAllMasterIdType();
        Optional<MasterIdType> optional=masterIdTypeList.stream().filter(i->i.getCode().equals(code)).findAny();
        if(optional.isPresent()){
            return optional.get();
        }
        return null;
    }
}
