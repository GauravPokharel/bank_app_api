package com.bankapplication.bankappapi.servicies.master.impl;

import com.bankapplication.bankappapi.entities.master.MasterTransactionType;
import com.bankapplication.bankappapi.repo.master.MasterTransactionTypeRepo;
import com.bankapplication.bankappapi.servicies.master.MasterTransactionTypeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MasterTransactionTypeServiceImpl implements MasterTransactionTypeService {
    private final MasterTransactionTypeRepo masterTransactionTypeRepo;

    public MasterTransactionTypeServiceImpl(MasterTransactionTypeRepo masterTransactionTypeRepo) {
        this.masterTransactionTypeRepo = masterTransactionTypeRepo;
    }

    @Override
    public List<MasterTransactionType> getAllMasterTransactionType() {
        return masterTransactionTypeRepo.findAll();
    }

    @Override
    public MasterTransactionType getMasterTransactionTypeById(int id) {
        return masterTransactionTypeRepo.getById(id);
    }

    @Override
    public Boolean checkMasterTransactionTypeById(int id) {
        List<MasterTransactionType> masterTransactionTypeList = getAllMasterTransactionType();
        Optional<MasterTransactionType> optional = masterTransactionTypeList.stream().filter(i -> i.getId() == id).findAny();
        if (optional.isPresent()) {
            return true;
        }
        return false;
    }

    @Override
    public void saveMasterTransactionType(MasterTransactionType masterTransactionType) {

        masterTransactionTypeRepo.save(masterTransactionType);
    }

    @Override
    public MasterTransactionType updateMasterTransactionType(MasterTransactionType masterTransactionType) {
        MasterTransactionType masterTransactionType1 = getMasterTransactionTypeById(masterTransactionType.getId());
        MasterTransactionType masterTransactionType2 = new MasterTransactionType();
        masterTransactionType2.setId(masterTransactionType.getId());
        masterTransactionType2.setCode(masterTransactionType1.getCode());
        masterTransactionType2.setName(masterTransactionType.getName());
        return masterTransactionTypeRepo.save(masterTransactionType2);
    }

    @Override
    public MasterTransactionType getMasterTransactionTypeByCode(String code) {
        List<MasterTransactionType> masterTransactionTypeList = getAllMasterTransactionType();
        Optional<MasterTransactionType> optional = masterTransactionTypeList.stream().filter(i -> i.getCode().equals(code)).findAny();
        if (optional.isPresent()) {
            return optional.get();
        }
        return null;
    }
}
