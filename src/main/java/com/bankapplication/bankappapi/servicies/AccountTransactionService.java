package com.bankapplication.bankappapi.servicies;

import com.bankapplication.bankappapi.dtos.*;
import com.bankapplication.bankappapi.entities.AccountTransaction;

import java.util.List;

public interface AccountTransactionService {
    List<TransactionDetailDto> getAllAccountTransaction();

    TransactionDetailDto getAccountTransactionById(int id);

    void depositTransaction(DepositDto depositDto);

    boolean withdrawTransaction(WithdrawDto withdrawDto);

    boolean topUpTransaction(TopUpDto topUpDto);

    boolean ibftTransaction(IBFTDto ibftDto);
}
