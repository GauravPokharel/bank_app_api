package com.bankapplication.bankappapi.servicies.impl;

import com.bankapplication.bankappapi.dtos.*;
import com.bankapplication.bankappapi.entities.Account;
import com.bankapplication.bankappapi.entities.AccountTransaction;
import com.bankapplication.bankappapi.repo.AccountTransactionRepo;
import com.bankapplication.bankappapi.servicies.AccountService;
import com.bankapplication.bankappapi.servicies.AccountTransactionService;
import com.bankapplication.bankappapi.servicies.master.MasterTransactionTypeService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AccountTransactionServiceImpl implements AccountTransactionService {
    private final AccountTransactionRepo accountTransactionRepo;
    private final AccountService accountService;
    private final MasterTransactionTypeService masterTransactionTypeService;

    public AccountTransactionServiceImpl(AccountTransactionRepo accountTransactionRepo, AccountService accountService, MasterTransactionTypeService masterTransactionTypeService) {
        this.accountTransactionRepo = accountTransactionRepo;
        this.accountService = accountService;
        this.masterTransactionTypeService = masterTransactionTypeService;
    }

    private void saveAccountTransaction(AccountTransaction accountTransaction) {
        accountTransactionRepo.save(accountTransaction);
    }

    private TransactionDetailDto accountTransactionToTransactionDetailDto(AccountTransaction accountTransaction) {
        return TransactionDetailDto.builder()
                .id(accountTransaction.getId())
                .transactionType(accountTransaction.getMasterTransactionType().getName())
                .accountNumber(accountTransaction.getAccount().getAccountNumber())
                .amount(accountTransaction.getAmount())
                .dateOfTransaction(accountTransaction.getDateOfTransaction())
                .toAccountNumber(accountTransaction.getToAccountNumber())
                .mobileNumber(accountTransaction.getToMobileNumber()).build();
    }

    @Override
    public List<TransactionDetailDto> getAllAccountTransaction() {
        List<TransactionDetailDto> transactionDetailDtoList = new ArrayList<>();
        List<AccountTransaction> accountTransactionList = accountTransactionRepo.findAll();
        for (AccountTransaction accountTransaction : accountTransactionList) {
            TransactionDetailDto transactionDetailDto = accountTransactionToTransactionDetailDto(accountTransaction);
            transactionDetailDtoList.add(transactionDetailDto);
        }
        return transactionDetailDtoList;
    }

    @Override
    public TransactionDetailDto getAccountTransactionById(int id) {
        Optional<AccountTransaction> optional = accountTransactionRepo.findAccountTransactionById(id);
        if (optional.isPresent()) {
            AccountTransaction accountTransaction = optional.get();
            return accountTransactionToTransactionDetailDto(accountTransaction);
        }
        return null;
    }

    @Override
    public void depositTransaction(DepositDto depositDto) {
        Account account = accountService.getAccountByAccountNumber(depositDto.getAccountNumber());
        double newBalance = account.getBalance() + depositDto.getAmount();
        account.setBalance(newBalance);
        AccountTransaction accountTransaction = AccountTransaction.builder()
                .account(account)
                .amount(depositDto.getAmount())
                .masterTransactionType(masterTransactionTypeService.getMasterTransactionTypeByCode("1001"))
                .dateOfTransaction(LocalDate.now()).build();
        accountService.saveAccount(account);
        saveAccountTransaction(accountTransaction);
    }

    @Override
    public boolean withdrawTransaction(WithdrawDto withdrawDto) {
        Account account = accountService.getAccountByAccountNumber(withdrawDto.getAccountNumber());
        if (account.getBalance() >= withdrawDto.getAmount()) {
            double newBalance = account.getBalance() - withdrawDto.getAmount();
            account.setBalance(newBalance);
            AccountTransaction accountTransaction = AccountTransaction.builder()
                    .account(account)
                    .amount(withdrawDto.getAmount())
                    .masterTransactionType(masterTransactionTypeService.getMasterTransactionTypeByCode("1002"))
                    .dateOfTransaction(LocalDate.now()).build();
            accountService.saveAccount(account);
            saveAccountTransaction(accountTransaction);
            return true;
        }
        return false;
    }

    @Override
    public boolean topUpTransaction(TopUpDto topUpDto) {
        Account account = accountService.getAccountByAccountNumber(topUpDto.getAccountNumber());
        if (account.getBalance() >= topUpDto.getAmount()) {
            double newBalance = account.getBalance() - topUpDto.getAmount();
            account.setBalance(newBalance);
            AccountTransaction accountTransaction = AccountTransaction.builder()
                    .account(account)
                    .amount(topUpDto.getAmount())
                    .masterTransactionType(masterTransactionTypeService.getMasterTransactionTypeByCode("1003"))
                    .dateOfTransaction(LocalDate.now())
                    .toMobileNumber(topUpDto.getMobileNumber())
                    .build();
            accountService.saveAccount(account);
            saveAccountTransaction(accountTransaction);
            return true;
        }
        return false;
    }

    @Override
    public boolean ibftTransaction(IBFTDto ibftDto) {
        Account account = accountService.getAccountByAccountNumber(ibftDto.getFromAccountNumber());
        if (account.getBalance() >= ibftDto.getAmount()) {
            double newBalance = account.getBalance() - ibftDto.getAmount();
            account.setBalance(newBalance);
            Account toAccount = accountService.getAccountByAccountNumber(ibftDto.getToAccountNumber());
            double newBalanceAdded = toAccount.getBalance() + ibftDto.getAmount();
            toAccount.setBalance(newBalanceAdded);
            AccountTransaction accountTransaction = AccountTransaction.builder()
                    .account(account)
                    .amount(ibftDto.getAmount())
                    .masterTransactionType(masterTransactionTypeService.getMasterTransactionTypeByCode("1004"))
                    .dateOfTransaction(LocalDate.now())
                    .toAccountNumber(ibftDto.getToAccountNumber())
                    .build();
            accountService.saveAccount(toAccount);
            accountService.saveAccount(account);
            saveAccountTransaction(accountTransaction);
            return true;
        }
        return false;
    }
}
