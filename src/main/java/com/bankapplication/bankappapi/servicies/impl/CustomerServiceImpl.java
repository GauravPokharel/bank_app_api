package com.bankapplication.bankappapi.servicies.impl;

import com.bankapplication.bankappapi.dtos.CustomerDto;
import com.bankapplication.bankappapi.entities.Customer;
import com.bankapplication.bankappapi.repo.CustomerRepo;
import com.bankapplication.bankappapi.servicies.AccountService;
import com.bankapplication.bankappapi.servicies.CustomerService;
import com.bankapplication.bankappapi.servicies.master.MasterIdTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.Period;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepo customerRepo;
    private final MasterIdTypeService masterIdTypeService;
    private final AccountService accountService;

    CustomerServiceImpl(CustomerRepo customerRepo, MasterIdTypeService masterIdTypeService, AccountService accountService) {
        this.customerRepo = customerRepo;
        this.masterIdTypeService = masterIdTypeService;
        this.accountService = accountService;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepo.findAll();
    }

    @Override
    public Customer saveCustomer(CustomerDto customerDto) {
        Customer customer = customerMapping(customerDto);
        customer.setAccount(accountService.createAccount(customerDto.getAccountType()));
        return customerRepo.save(customer);
    }


    private Customer customerMapping(CustomerDto customerDto) {
        return Customer.builder()
                .id(customerDto.getId())
                .fullName(customerDto.getFullName())
                .mobileNumber(customerDto.getMobileNumber())
                .emailAddress(customerDto.getEmailAddress())
                .dateOfBirth(customerDto.getDateOfBirth())
                .idValue(customerDto.getIdValue())
                .address((customerDto.getAddress()))
                .masterIdType(masterIdTypeService.getMasterIdTypeByCode(customerDto.getMasterIdTypeCode()))
                .parentName(customerDto.getParentName())
                .build();
       /* Customer customer = new Customer();
        customer.setId(customerDto.getId());
        customer.setFullName(customerDto.getFullName());
        customer.setMobileNumber(customerDto.getMobileNumber());
        customer.setParentName(customerDto.getParentName());
        customer.setEmailAddress(customerDto.getEmailAddress());
        customer.setDateOfBirth(customerDto.getDateOfBirth());
        customer.setIdValue(customerDto.getIdValue());
        customer.setAddress(customerDto.getAddress());
        customer.setMasterIdType(masterIdTypeService.getMasterIdTypeByCode(customerDto.getMasterIdTypeCode()));
        return customer;*/
    }

    @Override
    public Customer updateCustomer(CustomerDto customerDto) {
        Customer customer = updateCustomerMapping(customerDto);
        return customerRepo.save(customer);
    }

    private Customer updateCustomerMapping(CustomerDto customerDto) {

        Customer customer1 = getCustomerById(customerDto.getId());
        return Customer.builder()
                .id(customerDto.getId())
                .fullName(customerDto.getFullName())
                .mobileNumber(customerDto.getMobileNumber())
                .emailAddress(customerDto.getEmailAddress())
                .dateOfBirth(customerDto.getDateOfBirth())
                .idValue(customer1.getIdValue())
                .address((customerDto.getAddress()))
                .masterIdType(customer1.getMasterIdType())
                .parentName(customerDto.getParentName())
                .account(customer1.getAccount())
                .build();
       /* customer.setId(customerDto.getId());
        customer.setFullName(customerDto.getFullName());
        customer.setMobileNumber(customerDto.getMobileNumber());
        customer.setEmailAddress(customerDto.getEmailAddress());
        customer.setDateOfBirth(customerDto.getDateOfBirth());
        customer.setParentName(customerDto.getParentName());
        customer.setAddress(customerDto.getAddress());
        customer.setIdValue((customer1.getIdValue()));
        customer.setMasterIdType(customer1.getMasterIdType());
        customer.setAccount(customer1.getAccount());
        return customer;*/
    }

    @Override
    public Customer getCustomerById(int id) {
        return customerRepo.getById(id);
    }

    @Override
    public boolean checkId(int id) {

        return customerRepo.findCustomerById(id).isPresent();
    }

    @Override
    public boolean checkCustomerByMobileNumber(String number, Integer id) {
        if(id==null)
            return customerRepo.findCustomerByMobileNumber(number).isPresent();
        return customerRepo.findCustomerByMobileNumberAndIdNot(number,id).isPresent();
    }

    @Override
    public boolean checkCustomerByEmailAddress(String emailAddress, Integer id) {
        if (id == null)
            return !customerRepo.findCustomerByEmailAddress(emailAddress).isEmpty();
        return customerRepo.findCustomerByEmailAddressAndIdNot(emailAddress, id).isPresent();
    }

    @Override
    public boolean checkCustomerByIdentityValue(String idValue) {
        return customerRepo.findCustomerByIdValue(idValue).isPresent();
    }

    @Override
    public int calculateAge(LocalDate birthDate) {
        LocalDate currentDate = LocalDate.now();
        return Period.between(birthDate, currentDate).getYears();
    }

    public boolean isParentSet(CustomerDto customerDto) {
        return customerDto.getParentName() != null;
    }

    public boolean isMinor(LocalDate birthDate) {
        int age = calculateAge(birthDate);
        return age < 18;
    }

    @Override
    public List<Customer> searchCustomerByName(String searchValue) {
        return Collections.emptyList();
    }
}
