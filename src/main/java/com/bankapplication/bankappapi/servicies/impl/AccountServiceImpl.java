package com.bankapplication.bankappapi.servicies.impl;

import com.bankapplication.bankappapi.entities.Account;
import com.bankapplication.bankappapi.enums.AccountType;
import com.bankapplication.bankappapi.repo.AccountRepo;
import com.bankapplication.bankappapi.servicies.AccountService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {
    protected final AccountRepo accountRepo;

    AccountServiceImpl(AccountRepo accountRepo) {
        this.accountRepo = accountRepo;
    }

    @Override
    public void saveAccount(Account account) {
        accountRepo.save(account);
    }

    @Override
    public List<Account> getAllAccount() {
        return accountRepo.findAll();
    }

    @Override
    public Account getAccountById(int id) {
        Optional<?> optional = accountRepo.findAccountById(id);
        if (optional.isPresent()) {
            return (Account) optional.get();
        }
        return null;
    }

    @Override
    public Account createAccount(AccountType accountType) {
        Account account = new Account();
        account.setAccountType(accountType);
        LocalDate date = LocalDate.now();

        account.setOpenDate(date);
        String randomNum = "";
        boolean flag = true;
        while (flag) {
            randomNum = randomAccountNumberGeneration();

            if (accountType.name().equals("SAVING")) {
                randomNum = randomNum + "SA";

            } else if (accountType.name().equals("CURRENT")) {
                randomNum = randomNum + "CA";

            } else {
                randomNum = randomNum + "FD";
            }
            final String checkRandomNum = randomNum;
            List<Account> listAccount = accountRepo.findAll();
            List<Account> ac = listAccount.stream().filter(i -> i.getAccountNumber().equals(checkRandomNum)).collect(Collectors.toList());
            if (ac.isEmpty())
                flag = false;
        }
        if (!flag) {
            account.setAccountNumber(randomNum);
        }
        return account;
    }

    private String randomAccountNumberGeneration() {
        Random rnd = new Random();
        int number = rnd.nextInt(999999);

        // this will convert any number sequence into 6 character.
        return String.format("%06d", number);
    }

    @Override
    public Account getAccountByAccountNumber(String accountNumber) {
        /*List<Account> accountList = getAllAccount();
        Optional<?> optional = accountList.stream().filter(i -> i.getAccountNumber().equals(accountNumber)).findAny();*/
        Optional<Account> optional = accountRepo.findAccountByAccountNumber(accountNumber);
        if (optional.isPresent()) {
            return optional.get();
        }
        return null;
    }

    @Override
    public boolean isAccountNumberValid(String accountNumber) {
        return getAccountByAccountNumber(accountNumber) != null;
    }
}
