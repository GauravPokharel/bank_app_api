package com.bankapplication.bankappapi.configuration;

import com.bankapplication.bankappapi.dtos.GlobalApiResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<String> handleConstraintViolationException(ConstraintViolationException constraintViolationException) {
        final List<String> errors = new ArrayList<>();
        for (ConstraintViolation cv : constraintViolationException.getConstraintViolations()) {
            switch (cv.getMessageTemplate()) {
                case "NotNull":
                    errors.add(cv.getMessage());
                    break;
                case "NotBlank":
                    errors.add(cv.getMessage());
                    break;
                default:
                    errors.add(cv.getPropertyPath().toString() + ": " + cv.getMessage());
            }
        }
        return new ResponseEntity(new GlobalApiResponse(false, errors.get(0), errors), HttpStatus.BAD_REQUEST);

    }

    /*  @Override
      @ResponseBody
      protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
          return super.handleBindException(ex, headers, status, request);
      }*/
    @ResponseBody
    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return new ResponseEntity(new GlobalApiResponse(false, "Error occured", null), HttpStatus.BAD_REQUEST);
    }
}
