package com.bankapplication.bankappapi.entities.master;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Entity
@Table(name = "tbl_master_transaction_type")
@Getter
@Setter
public class MasterTransactionType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "name")
    @NotBlank(message = "Name of transaction type can't be empty")
    private String name;

    @Column(name = "code", unique = true)
    @NotBlank(message = "Code of transaction type can't be empty")
    @NotNull
    @Size(min = 2, max = 5, message = "Size of code should be between 2 and 5")
    @Positive(message = "Code should be greater than 10")
    private String code;
}
