package com.bankapplication.bankappapi.entities;

import com.bankapplication.bankappapi.entities.master.MasterTransactionType;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "tbl_account_transactions")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class AccountTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "account_id_id")
    private Account account;

    @OneToOne
    private MasterTransactionType masterTransactionType;

    @Column(name = "amount")
    private double amount;

    @Column(name = "cash_back_applicable")
    private boolean cashBackApplicable;

    @Column(name = "status")
    private String status;

    @Column(name = "to_account")
    private String toAccountNumber = "";

    @Column(name = "to_mobileNumber")
    private String toMobileNumber = "";

    @Column(name = "date_of_transaction")
    private LocalDate dateOfTransaction;
}
