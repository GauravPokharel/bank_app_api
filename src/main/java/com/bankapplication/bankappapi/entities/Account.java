package com.bankapplication.bankappapi.entities;

import com.bankapplication.bankappapi.enums.AccountType;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "tbl_account",uniqueConstraints = {
        @UniqueConstraint(name = "UNIQUE_account_number", columnNames = {"account_number"}),
})
@Getter
@Setter
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "account_number")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String accountNumber;

    @Column(name = "open_date")
    private LocalDate openDate;

    @Column(name = "close_date")
    private LocalDate closeDate;

    private double balance;

    @Column(name = "account_type")
    @Enumerated(EnumType.STRING)
    private AccountType accountType;

    @OneToOne(mappedBy = "account")
    @JsonManagedReference
    private Customer customer;
}


