package com.bankapplication.bankappapi.entities;

import com.bankapplication.bankappapi.entities.master.MasterIdType;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Entity
@Table(name = "tbl_customer", uniqueConstraints = {
        @UniqueConstraint(name = "UNIQUE_email_address", columnNames = {"email_address"}),
        @UniqueConstraint(name = "UNIQUE_mobile_number", columnNames = {"mobile_number"}),
        @UniqueConstraint(name = "UNIQUE_id_value", columnNames = {"id_value"})
})
@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false)
    private int id;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "mobile_number")
    @Positive(message = "Invalid mobile number")
    private String mobileNumber;

    @Email(message = "Input should be in email format")
    @Column(name = "email_address")
    @NotBlank(message = "Email address of customer can't be empty")
    private String emailAddress;

    @Column(name = "address")
    @NotBlank(message = "Address of customer type can't be empty")
    @Size(min = 3, max = 50, message = "Size must be between 3-50")
    private String address;

    @Column(name = "google_plus_code")
    private String googlePlusCode;

    @Column(name = "is_premium")
    private boolean isPremium;

    @Past
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "Date of birth field is mandatory")
    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @OneToOne()
    private MasterIdType masterIdType;

    @Column(name = "id_value")
    @NotBlank(message = "Identity code can't be empty")
    private String idValue;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    @JsonBackReference
    private Account account;

    @Column(name = "parent_name")
    private String parentName;
}
